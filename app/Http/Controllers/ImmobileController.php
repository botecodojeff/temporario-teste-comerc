<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Immobile;
use App\ImmobileNumber;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class ImmobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function welcome()
    {
        $immobileNumbers = DB::select(DB::raw("SELECT i1.* FROM immobile_number i1 WHERE i1.created_at = (SELECT MAX(i2.created_at) FROM immobile_number i2 WHERE i2.immobile_id = i1.immobile_id) AND i1.immobile_id IN (1, 2, 3) ORDER by i1.immobile_id"));

        return View('welcome')
            ->with('immobileNumbers', $immobileNumbers);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        $immobileNumbers = ImmobileNumber::all();

        return View('immobile.index')
            ->with('immobileNumbers', $immobileNumbers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        return View('immobile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        $rules = array(
            'number' => 'required|numeric',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('immobile/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $immobileNumber = new ImmobileNumber;
            $immobileNumber->number = Input::get('number');
            $immobileNumber->created_at = new \DateTime();
            $immobileNumber->immobile_id = Input::get('immobile');
            $immobileNumber->save();

            // redirect
            Session::flash('message', 'Imóvel criado com sucesso.');
            return Redirect::to('immobile');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        $immobileNumber = ImmobileNumber::find($id);

        return View('immobile.edit')
            ->with('immobileNumber', $immobileNumber);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        $rules = array(
            'number' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('nerds/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            $immobileNumber = ImmobileNumber::find($id);
            $immobileNumber->number = Input::get('number');
            $immobileNumber->created_at = new \DateTime();
            $immobileNumber->immobile_id = 1;
            $immobileNumber->save();

            Session::flash('message', 'Imóvel atualizado com sucesso!');
            return Redirect::to('immobile');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!Auth::check()) {
            return Redirect::to('login');
        }

        $immobileNumber = ImmobileNumber::find($id);
        $immobileNumber->delete();

        Session::flash('message', 'Imóvel removido com sucesso!');
        return Redirect::to('immobile');
    }

}
