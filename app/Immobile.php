<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Immobile extends Model
{
	protected $table = "immobile";

    public function ImmobileNumbers() {
        return $this->hasMany('App\ImmobileNumber');
    }
}
