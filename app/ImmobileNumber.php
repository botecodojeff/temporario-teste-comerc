<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImmobileNumber extends Model
{
	protected $table   = "immobile_number";
	public $timestamps = false;
	protected $dates   = ['created_at'];

    public function Immobile() {
        return $this->belongsTo('App\Immobile');
    }
}
