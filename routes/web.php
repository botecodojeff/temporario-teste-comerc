<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'ImmobileController@welcome')->name('welcome');
Route::get('/immobile', 'ImmobileController@index')->name('index');
Route::get('/immobile/create', 'ImmobileController@create')->name('immobile.create');
Route::post('/immobile', 'ImmobileController@store')->name('immobile.store');
Route::get('/immobile/{id}/edit', 'ImmobileController@edit')->name('immobile.edit');
Route::put('/immobile/{id}', 'ImmobileController@update')->name('immobile.update');
Route::delete('/immobile/{id}', 'ImmobileController@destroy')->name('immobile.destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
