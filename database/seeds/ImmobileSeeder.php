<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ImmobileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('immobile')->insert([
	        ['id' => 1, 'name' => "Imoveis vendidos"],
	        ['id' => 2, 'name' => "Imoveis alugados"],
	        ['id' => 3, 'name' => "Imoveis disponíveis"],
        ]);
    }
}
