<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
	        ['id' => 1, 'name' => "Admin", 'email' => "admin@admin.com.br", 'password' => "$2y$10$4SGf5NHBZrcaK2s/1x44wOBRKCJKxwuOdi/R7gHuDvrxMrKj.cEzm", 'remember_token' => "8IipXbZtbyUJKNLQSDT3TXOGt5BudHTSvUwAk0IMoS2WOnWLiztX59KvB3rE", 'created_at' => "2018-02-24 18:46:42", 'updated_at' => "2018-02-24 18:46:42"],
        ]);
    }
}
