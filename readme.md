# Comerc Energia — Teste de código com Laravel

## Considerações iniciais

Não sei exatamente como se dará a avaliação deste teste, todavia, acho importante frisar que por mais simples que seja, ele precisa ter um tempo adequado para a entrega. Eu havia informado que só poderia iniciá-lo no sábado e o prazo estipulado para a entrega foi o próprio sábado, dia 24/02 às 20h. Tive menos de 24h para realizar. Minha sincera opinião é que dificilmente o potencial completo do candidato poderá ser avaliado num prazo tão curto, uma vez que partes do escopo serão entregues sem o devido cuidado e eventualmente sem as corretas boas práticas de desenvolvimento (que exigem mais tempo).

De qualquer forma, sem melindres, mesmo não conhecendo o Laravel, aceitei o desafio e aqui está o código. Gostaria de frisar que:

1. Houve pouquíssima preocupação com o layout e usabilidade, dado o curtíssmo prazo.
2. As rotas restritas não estão "trancadas" da maneira mais elegante. Suponho que o Laravel tenha uma forma mais inteligente, eventualmente direto no arquivo de configuração das rotas. Em Symfony, por exemplo, existe um arquivo de configuração específico para tratar disso (securty.yml).
3. A coleta do clima/tempo via RSS não foi efetuado devido a falta de tempo. Chequei que existem alguns bons bundles para esse trabalho no Laravel, mas precisaria de mais tempo para fazer.
4. Preferi usar duas tabelas com relacionamento ManyToOne para ter a chance de mostrar mais habilidade com a estruturação da base de dados e com querys mais avançadas.



## Instalação



### 1. Clone o projeto


```sh
git clone git@bitbucket.org:botecodojeff/temporario-teste-comerc.git
```


### 2. Instale as dependências do Laravel

```sh
composer install
```

(caso o **composer** não esteja disponível, baixe-o pelo site [getcomposer.org](http://getcomposer.org))


### 3. Configure o banco de dados
Verifique a documentação em [https://laravel.com](https://laravel.com)


### 4. Instale os seeders (extremamente importante para o funcionamento do projeto)

```sh
php artisan db:seed --class=ImmobileSeeder
php artisan db:seed --class=UserSeeder
```


### 5. Dados para acesso ao sistema (após instalar os seeders!)

- **Dashboard:** /
- **Admin:** /immobile
- **Usuário:** admin@admin.com.br
- **Senha:** admin123

- **URL para acesso via web:** [http://comerc.falecomjeff.com/](http://comerc.falecomjeff.com/)
- **Admin via web:** [http://comerc.falecomjeff.com/immobile](http://comerc.falecomjeff.com/immobile) (Usuário e senha são os mesmos que foram listados acima).


### 6. Contato

- falecomjeff@gmail.com
- (11) 99635-5400
