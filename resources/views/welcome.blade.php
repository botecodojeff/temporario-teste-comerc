<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../../../favicon.ico">

        <title>Teste Comerc Energia</title>

        <!-- Bootstrap core CSS -->
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    </head>
<body>
<br><br>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <div class="alert alert-success">
                <p>Imóveis vendidos</p>
                <h1>
                    @if($immobileNumbers)
                        {{ $immobileNumbers[0]->number }}
                    @else
                        0
                    @endif
                </h1>
            </div>

            <div class="alert alert-success">
                <p>Imóveis alugados</p>
                <h1>
                    @if($immobileNumbers)
                        {{ $immobileNumbers[1]->number }}
                    @else
                        0
                    @endif
                </h1>
            </div>

            <div class="alert alert-success">
                <p>Imóveis disponíveis</p>
                <h1>
                    @if($immobileNumbers)
                        {{ $immobileNumbers[2]->number }}
                    @else
                        0
                    @endif
                </h1>
            </div>
        </div>
        <div class="col-sm-9">
            <div class="col-sm-7">
                Lorem ipsum dolor sit amet,
                consectetur adipiscing elit
                Duis nec tellus vel nibh maximus
                condimentum a facilisis mauris. Vivamus
                commodo sodales pellentesque. Etiam eget
                sem quis justo maximus pellentesque. Donec
                orci quam, pulvinar a porta sit amet, tempus
                in est. Proin tellus neque, condimentum vel
                turpis nec, sodales convallis arcu.
            </div>
            <div class="col-sm-5">
                <img src="{{ asset('img/foto.png') }}" width="200" height="200">
            </div>
        </div>
    </div>
    <div class="row alert alert-success">
        <div class="col-sm-10">{{ date('H:i') }} | São Paulo - 20º</div>
        <div class="col-sm-2">Logo da empresa</div>
    </div>
</div>

</body>
</html>