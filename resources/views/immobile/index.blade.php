<!DOCTYPE html>
<html>
<head>
    <title>Teste Comerc Energia</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('immobile') }}">Logotipo</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('immobile') }}">Visualizar imóveis</a></li>
            <li><a href="{{ URL::to('immobile/create') }}">Inserir quantidade de imóveis</a>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </nav>

    <h1>Todos os imóveis</h1>

    <!-- will be used to show any messages -->
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <td>ID</td>
                <td>TÍTULO</td>
                <td>VALOR</td>
                <td>DATA/HORA</td>
                <td>AÇÕES</td>
            </tr>
        </thead>
        <tbody>
        @foreach($immobileNumbers as $immobileNumber)
            <tr>
                <td>{{ $immobileNumber->id }}</td>
                <td>{{ $immobileNumber->immobile->name }}</td>
                <td>{{ $immobileNumber->number }}</td>
                <td>{{ $immobileNumber->created_at->format('d/m/Y H:i:s') }}</td>

                <!-- we will also add show, edit, and delete buttons -->
                <td>
                    <!-- edit this nerd (uses the edit method found at GET /immobile/{id}/edit -->
                    <a class="btn btn-small btn-info" href="{{ URL::to('immobile/' . $immobileNumber->id . '/edit') }}">Editar</a>

                    <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                    <!-- we will add this later since its a little more complicated than the other two buttons -->
                    {{ Form::open(array('url' => 'immobile/' . $immobileNumber->id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Remover', array('class' => 'btn btn-small btn-warning')) }}
                    {{ Form::close() }}

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
</body>
</html>