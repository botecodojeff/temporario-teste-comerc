<!DOCTYPE html>
<html>
<head>
    <title>Teste Comerc Energia</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('immobile') }}">Logotipo</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('immobile') }}">Visualizar imóveis</a></li>
            <li><a href="{{ URL::to('immobile/create') }}">Inserir quantidade de imóveis</a>
        </ul>
    </nav>

<h1>Editar <strong>{{ $immobileNumber->immobile->name }} (ID {{ $immobileNumber->id }})</strong> </h1>

<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($immobileNumber, array('route' => array('immobile.update', $immobileNumber->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('number', 'Number') }}
        {{ Form::text('number', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}
    <a class="btn btn-small btn-info" href="{{ URL::to('immobile/') }}">Voltar</a>

{{ Form::close() }}

</div>
</body>
</html>