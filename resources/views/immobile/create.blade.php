<!DOCTYPE html>
<html>
<head>
    <title>Teste Comerc Energia</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

    <nav class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('immobile') }}">Logotipo</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="{{ URL::to('immobile') }}">Visualizar imóveis</a></li>
            <li><a href="{{ URL::to('immobile/create') }}">Inserir quantidade de imóveis</a>
        </ul>
    </nav>

<h1>Criar uma nova quantidade para um imóvel</h1>

<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::open(array('url' => 'immobile')) }}

    <div class="form-group">
        {{ Form::label('immobile', 'Tipo de imóvel') }}
        {{ Form::select('immobile', array(1 => 'Imóveis vendidos', 2 => 'Imóveis alugados', 3 => 'Imóveis disponíveis'), Input::old('immobile'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('number', 'Valor') }}
        {{ Form::text('number', Input::old('number'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Create', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

</div>
</body>
</html>